# coding=utf-8
import json
import socket
import sys
import math

class EbinBot(object):
    
    color = 'pilu'
    angle = 0.0
    maximumDriftAngle = 50
    track = json.loads('[{"lol":"4"}]')
    pieceIndex = 0
    inPieceDistance = 0.0
    distanceLeftOnPiece = 0.0
    mutkingsMe = False
    gameTick = 0
    
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        self.throttle(0.5)

    def on_crash(self, data):
        print data['name'] + ' crashed'
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
                
            if 'gameTick' in msg:
                self.gameTick = msg['gameTick']
                
            #oma väri talteen
            if msg_type == 'yourCar':
                self.color = data['color']
                #test print
                print self.color
                
            #kisa käynnissä
            elif msg_type == 'carPositions':
                for i in data:
                    #oma auto
                    if i['id']['color'] == self.color:
                        self.angle = i['angle']
                        self.pieceIndex = i['piecePosition']['pieceIndex']
                        self.inPieceDistance = i['piecePosition']['inPieceDistance']
                        
            #radan tiedot talteen
            elif msg_type == 'gameInit':
                self.track = data['race']['track']['pieces']
                
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()

            #ollaan suoralla
            if 'length' in self.track[self.pieceIndex]:
                #kuinka monta prosenttia suorasta ajettu
                self.distanceLeftOnPiece = 1 - self.inPieceDistance/self.track[self.pieceIndex]['length']
            #ollaan mutkassa
            elif 'angle' in self.track[self.pieceIndex]:
                #matematiikka ei toimi
                #self.distanceLeftOnPiece = 1 - self.inPieceDistance/(self.track[self.pieceIndex]['angle']/360)*2*math.pi*self.track[self.pieceIndex]['radius']
                a = 3
                
            if self.pieceIndex + 1 < len(self.track):
                #mutka seuraavassa blokissa
                if 'angle' in self.track[self.pieceIndex + 1]:
                    self.mutkingsMe = True
                else:
                    self.mutkingsMe = False
                
            self.angle = abs(self.angle)
            
            if self.angle > self.maximumDriftAngle:
                print self.angle
                self.throttle(0.0)
            else:
                driftPercent = self.angle/self.maximumDriftAngle
                throttleAmount = 1.0 - 1.0 * driftPercent
                if(self.mutkingsMe):
                    throttleAmount = throttleAmount*self.distanceLeftOnPiece
                self.throttle(throttleAmount)

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = EbinBot(s, name, key)
        bot.run()
